<!DOCTYPE html>
<!--[if lt IE 9 ]><html class="no-js oldie" lang="en"> <![endif]-->
<!--[if IE 9 ]><html class="no-js oldie ie9" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!-->
<html class="no-js" lang="en">
<!--<![endif]-->

<head>
    <script src="https://cdn.jsdelivr.net/particles.js/2.0.0/particles.min.js"></script>

    <script>
        particlesJS.load('top', '{\n' +
            '    "particles":{\n' +
            '        "number":{\n' +
            '            "value":80\n' +
            '        },\n' +
            '        "color":{\n' +
            '            "value":"#f9f3f4"\n' +
            '        },\n' +
            '        "shape":{\n' +
            '            "type":"circle",\n' +
            '            "stroke":{\n' +
            '                "width":1,\n' +
            '                "color":"#ccc"\n' +
            '            }\n' +
            '        },\n' +
            '        "opacity":{\n' +
            '            "value":0.5,\n' +
            '            "random":true\n' +
            '        },\n' +
            '        "size":{\n' +
            '            "value":2\n' +
            '        },\n' +
            '        "line_linked":{\n' +
            '            "enable":true,\n' +
            '            "distance":110\n' +
            '        },\n' +
            '        "move":{\n' +
            '            "enable":true,\n' +
            '            "speed": 1\n' +
            '        }\n' +
            '    }\n' +
            '}',
            function(){
                console.log('particles.json loaded...')
            })
    </script>
    <!--- basic page needs
    ================================================== -->
    <meta charset="utf-8">
    <title>Tejas Futuristics</title>
    <meta name="description" content="">
    <meta name="author" content="">
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/particlesjs/2.2.2/particles.js"></script>

    <!-- mobile specific metas
    ================================================== -->
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSS
    ================================================== -->
    <link rel="stylesheet" href="https://colorlib.com/etc/glint/css/base.css">
    <link rel="stylesheet" href="https://colorlib.com/etc/glint/css/vendor.css">
    <link rel="stylesheet" href="https://colorlib.com/etc/glint/css/main.css">

    <!-- script
    ================================================== -->
    <script src="https://colorlib.com/etc/glint/js/modernizr.js"></script>
    <script src="https://colorlib.com/etc/glint/js/pace.min.js"></script>

    <!-- favicons
    ================================================== -->

</head>

<body id="top">

<!-- header
    ================================================== -->
<header class="s-header">

    <div class="header-logo">
        <a class="site-logo" href="index.html">

        </a>
    </div>

    <nav class="header-nav">

        <a href="#0" class="header-nav__close" title="close"><span>Close</span></a>

        <div class="header-nav__content">
            <h3>Navigation</h3>

            <ul class="header-nav__list">
                <li class="current"><a class="smoothscroll"  href="#home" title="home">CRES Home</a></li>
                <li><a class="smoothscroll"  href="#about" title="about">CRES AUTO</a></li>
                <li><a class="smoothscroll"  href="#services" title="services">CRES HEALTH</a></li>
                <li><a class="smoothscroll"  href="#services" title="services">TEAM</a></li>
                <li><a class="smoothscroll"  href="#services" title="services">Contact Us</a></li>
                <li><a class="smoothscroll"  href="#services" title="services">Support</a></li>

            </ul>



            <ul class="header-nav__social">
                <li>
                    <a href="#"><i class="fa fa-facebook"></i></a>
                </li>
                <li>
                    <a href="#"><i class="fa fa-twitter"></i></a>
                </li>
                <li>
                    <a href="#"><i class="fa fa-google"></i></a>
                </li>

            </ul>

            <br><BR><Br><Br>

            <ui><a class="smoothscroll"  href="#Careers" title="Careers">Careers </a></ui><br>
            <ui><a class="smoothscroll"  href="#Policies" title="Policies">Policies</a></ui>
        </div> <!-- end header-nav__content -->

    </nav>  <!-- end header-nav -->

    <a class="header-menu-toggle" href="#0">

        <span class="header-menu-text">Menu</span>
        <span class="header-menu-icon"></span>
    </a>
    <font >

        <a href=""> <span class="header-menu-text">Support</span></a>
    </font>
</header> <!-- end s-header -->


<!-- home
    ================================================== -->
<section id="home" class="s-home target-section" data-parallax="scroll" data-image-src="${resource(dir: 'images', file: 'hello.jpg')}" data-natural-width=3000 data-natural-height=2000 data-position-y=center>

    <div class="overlay"></div>
    <div class="shadow-overlay"></div>

    <div class="home-content">

        <div class="row home-content__main">

            <h3>Welcome to Tejas Futuristics</h3>

            <h6>
            <font color="white">    Established in 2018, Tejas Futuristics Private Limited is based in Navi Mumbai, Maharashtra, India.
            We aim to harness technology to solve everyday problems and make life easier,
            safer and more convenient for the common masses.
            Tejas Futuristics is working in different spheres of the automation industry specializing
            in Home, Automobiles and Health. We  thrive to provide simple and effective solutions to
            the end consumers and businesses together
            </font>
            </h6>
            <div class="home-content__buttons">
                <a href="/dashboard" class="btn btn--stroke">
                    Login
                </a>
                <a href="/user" class="btn btn--stroke">
                    Signup
                </a>
            </div>

            <BR><BR><BR><BR>

        </div>





    </div> <!-- end home-content -->


    <ul class="home-social">
        <li>
            <a href="#0"><i class="fa fa-facebook" aria-hidden="true"></i><span>Facebook</span></a>
        </li>
        <li>
            <a href="#0"><i class="fa fa-twitter" aria-hidden="true"></i><span>Twiiter</span></a>
        </li>
        <li>
            <a href="#0"><i class="fa fa-google" aria-hidden="true"></i><span>Google</span></a>
        </li>

    </ul>
    <!-- end home-social -->

</section> <!-- end s-home -->





<!-- photoswipe background
    ================================================== -->
<div aria-hidden="true" class="pswp" role="dialog" tabindex="-1">

    <div class="pswp__bg"></div>
    <div class="pswp__scroll-wrap">

        <div class="pswp__container">
            <div class="pswp__item"></div>
            <div class="pswp__item"></div>
            <div class="pswp__item"></div>
        </div>

        <div class="pswp__ui pswp__ui--hidden">
            <div class="pswp__top-bar">
                <div class="pswp__counter"></div><button class="pswp__button pswp__button--close" title="Close (Esc)"></button> <button class="pswp__button pswp__button--share" title=
            "Share"></button> <button class="pswp__button pswp__button--fs" title="Toggle fullscreen"></button> <button class="pswp__button pswp__button--zoom" title=
            "Zoom in/out"></button>
                <div class="pswp__preloader">
                    <div class="pswp__preloader__icn">
                        <div class="pswp__preloader__cut">
                            <div class="pswp__preloader__donut"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
                <div class="pswp__share-tooltip"></div>
            </div><button class="pswp__button pswp__button--arrow--left" title="Previous (arrow left)"></button> <button class="pswp__button pswp__button--arrow--right" title=
        "Next (arrow right)"></button>
            <div class="pswp__caption">
                <div class="pswp__caption__center"></div>
            </div>
        </div>

    </div>

</div> <!-- end photoSwipe background -->


<!-- preloader
    ================================================== -->
<div id="preloader">
    <div id="loader">
        <div class="line-scale-pulse-out">
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
        </div>
    </div>
</div>


<!-- Java Script
    ================================================== -->
<script src="https://colorlib.com/etc/glint/js/jquery-3.2.1.min.js"></script>
<script src="https://colorlib.com/etc/glint/js/plugins.js"></script>
<script src="https://colorlib.com/etc/glint/js/main.js"></script>

</body>

</html>