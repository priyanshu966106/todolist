<html>
<g:render template="header"/>
<body ng-app="todoApp">
<div class="container-fluid">
    <div class="row">
        <div class="col-sm-3 col-md-2 sidebar">
            <ul class="nav nav-sidebar">
                <li class="active" ><%= link(action:'index',controller:'dashboard') { '<span class="glyphicon glyphicon-list"></span> Dashboard' } %></li>

                <li ><%= link(action:'settings',controller:'dashboard') { '<span class="glyphicon glyphicon-cog"></span> Settings' } %> </li>
            </ul>
        </div>

</body>
</div>
</div>
</div>
</html>