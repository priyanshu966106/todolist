/*
Author-Priyanshu Kumar
Email-priyanshu966106@gmail.com
*/

//creating an application module
var todoAppModule = angular.module("todoApp", [ ]);
//services
todoAppModule.factory("dataFac", ["$http",function($http){ //datafactory service
    var data = {};

    data.fetchLabelDetails = function(){ //fetch label details
        return $http.get("/api/label/allabel/");
    };
    data.fetchTtaskDetails = function(){ //fetch  Todays task  details
        return $http.get("/api/task/ttask/");
    };
    data.fetchAtaskDetails = function(){ //fetch all task details
        return $http.get("/api/task/atask/");
    };
    data.deleteTask = function(taskid){ // post to delete a particular task
        return $http.post("/api/task/deltask/",taskid);
    };
    data.markComplete = function(taskid){ //mark a task complete
        return  $http.post("/api/task/updatetask/",taskid);
    };
    data.addTask = function(taskdet){ //post to add task
        return  $http.post("/api/task/savetask/",taskdet);
    };
    data.changeLabel = function(clabel){ //post request to change label of exisitng task
        return  $http.post("/api/task/chlabel/",clabel);
    };
    data.changePriority = function(cpri){ //post request to change priority of exsisiting task
        return $http.post("/api/task/chpriority/",cpri);
    };
    data.setupTsa = function(){ //setup two step verification authentication
        return $http.get("/api/settings/gettsa/");
    };
    data.signup = function(nuser){ //post to signup new user
        return $http.post("/user/create/",nuser);
    };
    data.productivityGraph = function(){ //get productiity graph details
        return   $http.get("/api/graph/index/");
    };
    data.getLabel = function(){
        return $http.get("/api/label/allabel/");
    };
    data.deleteLabel = function(labelid){ //delete label details
        return $http.post("/api/label/dellabel/",labelid);
    };
    data.addLabel = function(label){ // add new label
        return  $http.post("/api/label/savelabel/",label);
    };
    data.getNumber = function(){ //get user current phone number
        return  $http.get("/api/settings/getnumber/");
    };
    data.deactivateTsaFlow = function(){ //deactivate two step authentication flow
        return  $http.get("/api/settings/deactivatetsa/");
    };
    data.activateTsaFlow = function(){ //activate two step authentication flow
        return  $http.get("/api/settings/activatetsa/");
    };
    data.changeEmail = function(nemail){ //change user email address
        return  $http.post("/api/settings/cemail/",nemail);
    };
    data.changePassword = function(puser){ //  post request to change user current password
        return $http.post("/api/settings/cpass/", puser);
    };
    data.changePhoneno= function(puser){ //post request to change user current phone number
        return $http.post("/api/settings/cphone/",puser);
    };
    data.deactivateAccount = function(drec){ //deacivate user account
        return $http.post("/api/settings/drec/",drec);
    };

    return data;
}]);
todoAppModule.factory("checktsa", ["$http",function($http){
    return {
        getTsa: function() { // setup two step authentication flow

            $http.get("/api/settings/gettsa/") //reading data from api
                .success(function (data) {
                    var tsao=data;
                    if(tsao.tsa==true){ //checking two step authentication enabled or not

                        if(tsao.tsacurr==true){ //check two step authentication login flow
                            window.location="/dashboard/authen/"; //redirecting to authentication page
                        }
                    }


                })
                .error(function (data, status) {
                    console.error("failure loading  record", status, data);
                    //return blank record if something goes wrong
                });


        }
    };
}]);
//controllers
todoAppModule.controller("taskCtrl", function($scope, $http,$filter,$interval,dataFac,$timeout,checktsa) {

    //initialisation
    $scope.showLoader=true;
    $scope.sortType="";
    $scope.sortReverse=false;
    $scope.spi=0;
    $scope.curpage=0;
    $scope.tdate=new Date();
    $scope.setMode=function (mode) {
        $scope.taskMode=mode;
    };


    checktsa.getTsa(); //check two step authentication

    function getlabel() //get label details
    {
        dataFac.fetchLabelDetails().success(function(response){
            $scope.labeldata = response;

        });

    }
    getlabel();
    $timeout(function () { //deleay by 0.5 sec
        updatettask();
    }, 500);

    function updatettask() { //update the all task and task
        console.log($scope.taskMode);
        if($scope.taskMode==1) {
            dataFac.fetchTtaskDetails().success(function (response) {
                $scope.tdetail = response;
                $scope.tasklen = $scope.tdetail.ttasks.length;
                if ($scope.tasklen == 0) {
                    $scope.numpage = 0;
                }
                else {
                    $scope.numpage = ($scope.tasklen / 6) + 1;
                    $scope.numpage = parseInt($scope.numpage);

                }
                if ($scope.numpage != 0) {
                    if ($scope.curpage + 1 > $scope.numpage) {
                        $scope.pageset($scope.curpage - 1);
                    }
                }
                // binding the data to the $scope variable
                $scope.showLoader = false;

            })
                .error(function (response) {
                    console.log(response);
                    $scope.tdetail = {}; //return blank record if something goes wrong
                });
        }
        else {
            dataFac.fetchAtaskDetails() //reading the .json file
                .success(function (data) {
                    $scope.tadetail = data;
                    // binding the data to the $scope variable
                    $scope.showLoader=false;
                    $scope.tasklen=$scope.tadetail.atasks.length;

                    if($scope.tasklen==0) {
                        $scope.numpage=0;
                    }
                    else
                    {
                        $scope.numpage = ($scope.tasklen / 6)+1;
                        $scope.numpage = parseInt($scope.numpage);

                    }
                    if($scope.numpage!=0){
                        if($scope.curpage+1>$scope.numpage)
                        {
                            $scope.pageset($scope.curpage-1);
                        }}


                })
                .error(function (data) {
                    console.error("failure loading the  record",data);
                    $scope.tadetail = {}; //return blank record if something goes wrong
                });
        }
    }


    $scope.tdelete = function() {  //delete task

        dataFac.deleteTask($scope.tid).success(function(response){
            //post data to the api
            updatettask();
            swal(response.message, "", response.code); //using sweet aleart

        }).error(function(response) {
            console.log(response);
        });



    };
    $scope.markc = function() {  //mark task as completed
        dataFac.markComplete($scope.tid).success(function(response){
            //post data to the api
            updatettask();
            swal(response.message,"",response.code);
        }).error(function(response) {
            console.log(response);

        });
    };
    $scope.addtask = function() { //add a new task

        $scope.task.tdur = $filter("date")($scope.task.tdur, "yyyy-MM-dd");
        dataFac.addTask($scope.task).success(function(response){//post data to the api
            updatettask();
            swal(response.message,"",response.code);
            $("#taskModal").modal("hide");
        }).error(function(response) {
            console.log(response);

        });


    };
    $scope.setsorttype=function (data) { //sortType Asc or Desc

        $scope.sortType=data;
    };
    $scope.sorting=function (order,data2) { //sort by selector
        $scope.sortReverse=order;

        if(data2==0) {
            $scope.setsorttype("priority");
        }
        else if(data2==1)
        {
            $scope.setsorttype("name");
        }
        else if(data2==3)
        {
            $scope.setsorttype("status");
        }
        else if(data2==4)
        {
            $scope.setsorttype("date");
        }
        else if(data2==5)
        {
            $scope.setsorttype("priority");
        }
        else
        {
            $scope.setsorttype("label");
        }

    };
    $scope.changelabel=function () { //change the current label


        $scope.clabel.taskno=$scope.selectask;

        dataFac.changeLabel($scope.clabel)
        //post data to the api
            .success(function(response) {
                updatettask();
                swal(response.message,"",response.code);
                $("#labelmodal").modal("hide");
            }).error(function(response) {
            console.log(response);
        });



    };
    $scope.changePriority=function () { //change current priority

        $scope.cpri.taskno=$scope.selectask;
        dataFac.changePriority($scope.cpri)
        //post data to the api
            .success(function(response) {
                updatettask();
                swal(response.message,"",response.code);
                $("#priorityModal").modal("hide");
            }).error(function(response) {
                console.log(response);
        });



    };

    $scope.cmodal=function (taskid) { //initialise change label modal

        $scope.selectask=taskid;

        $("#labelmodal").modal("show");

    };
    $scope.pmodal=function (taskid) { //initialise change priority modal

        $scope.selectask=taskid;

        $("#priorityModal").modal("show");

    };



    $scope.getNumber = function(num) { //change no of pages to array
        return new Array(num);
    };
    $scope.pageset=function (pageno) { //count no of pages
        $scope.spi=5*pageno;
        $scope.curpage=pageno;
    };

    $scope.prevpage=function () { //goto previous page
        if($scope.curpage!=0) {
            $scope.curpage = $scope.curpage - 1;
            $scope.pageset($scope.curpage);
        }
        else {
            swal("Displaying", "page 1 of" + $scope.numpage, "info"); //error if on 1st page
        }
    };
    $scope.nextpage=function () { //goto next page
        if(($scope.curpage+1)!=$scope.numpage) {
            $scope.curpage = $scope.curpage + 1;
            $scope.pageset($scope.curpage);
        }
        else
        {
            swal("Displaying","Page " + $scope.numpage+ "of" +  $scope.numpage,"info"); //error if on last page
        }
    };
    $scope.activeset=function (pageno) { //set active page

        if($scope.curpage==pageno) {

            return "active";
        }
        else {
            return null;
        }
    };

});
todoAppModule.controller("signupCtrl", function($scope, $rootScope, $http, $location,$window,dataFac){
    $scope.signup = function () {
        dataFac.signup($scope.nuser) //post data to the api
            .success(function(data) {
                if(data.id=="error") {

                    $scope.nuser.errmsg = data.errmsg;
                }
                else {

                    $window.location.href = "user/signup";

                }
            }).error(function(response) {
            console.log(response);

        });
    };

});//end
todoAppModule.controller("graphCtrl", function($scope, $rootScope, $http, $location,$window,dataFac,checktsa){

    checktsa.getTsa();


    dataFac.productivityGraph()//reading data from the api
        .success(function (data) {

            $scope.graphdata=data;
            console.log(data);
            // binding the data to the $scope variable
        })
        .error(function (data) {
            console.log(data);
        });
    //plotting the google graphs
    google.charts.load("current", {"packages":["line"]});
    google.charts.setOnLoadCallback(drawChart);

    function drawChart() {

        var data = new google.visualization.DataTable();
        data.addColumn("number", "Day");
        data.addColumn("number", "Productivity");


        data.addRows([
            [1,  $scope.graphdata[0] ],
            [2,$scope.graphdata[1]  ],
            [3, $scope.graphdata[2]  ],
            [4,  $scope.graphdata[3] ],
            [5,  $scope.graphdata[4] ],
            [6,   $scope.graphdata[5] ],
            [7,  $scope.graphdata[6] ]

        ]);

        var options = {
            chart: {
                title: "Productivity Graph",
                subtitle: "your productivity this week "
            },
            width: 900,
            height: 500,
            axes: {
                x: {
                    0: {side: "top"}
                }
            }
        };

        var chart = new google.charts.Line(document.getElementById("line_top_x"));

        chart.draw(data, google.charts.Line.convertOptions(options));
    }


});//end
todoAppModule.controller("displaylabelCtrl", function($scope, $http,$interval,dataFac,checktsa) {
    $scope.showLoader=true;
    checktsa.getTsa();

    $interval(callAtInterval, 1000);//update the tasks
    function callAtInterval()
    {
        dataFac.getLabel() //reading the label .json file
            .success(function (data) {
                $scope.ldetail = data;
                // binding the data to the $scope variable
                $scope.showLoader=false;
            })
            .error(function (data, status) {
                console.error("failure loading the student record", status, data);
                $scope.ldetail = {}; //return blank record if something goes wrong
            });
    }

    $scope.ldelete = function() {  //delete label

        dataFac.deleteLabel($scope.lid)
        //post data to the api
            .success(function(data) {


                swal(data.message,"", data.code); //using sweet aleart for delete


            }).error(function(data) {
                console.log(data);

        });

    };
    $scope.addlabel = function() {  //add label

        dataFac.addLabel($scope.label)
        //post data to the api
            .success(function(data) {
                swal(data.message,"", data.code); //using sweet aleart for delete
            }).error(function(data) {
            console.log(data);

        });



    };



});
todoAppModule.controller("tsvCtrl", function($scope, $http,dataFac) {


    var number="";
    var dnumber="";
    $scope.dno="";
    $scope.vcode="";
    var purl="https://api.ringcaptcha.com/";




    dataFac.getNumber() //reading the .json file
        .success(function (data) {
            number=data.message;
            console.log(number);
            $scope.selnum=data.message;
            dnumber=number.toString().slice(8,12);
            $scope.dno=dnumber;
            $scope.sendcode(number);


        })
        .error(function (data) {
            console.log(data);
        });




    var config = {
        headers : {
            "Content-Type": "application/x-www-form-urlencoded;charset=utf-8;"
        }
    };












    dataFac.fetchAtaskDetails() //reading the .json file
        .success(function (data) {
            $scope.tdetail = data;
            // binding the data to the $scope variable
            $scope.showLoader=false;
        })
        .error(function (data, status) {
            console.error("failure loading the  record", status, data);
            $scope.tdetail = {}; //return blank record if something goes wrong
        });








    $scope.sendcode=function (data) {
        console.log($scope.dno);
        var euser= $.param({
            app_key : "e7a8yqonomo7o6a4u6o8",
            service : "SMS",
            api_key : "f642c1cd3145f4c172e7668e7a2cec3feefc52aa",
            phone :data
        });

        var fvurl=purl+euser.app_key+"/code/"+euser.service;
        $http.post(fvurl,euser,config)
        //post data to the api
            .success(function(data, status, headers, config) {


                console.log(data);


            }).error(function(data, status, headers, config) {
            console.log(data);

        });



    };
    $scope.deactivatelflow=function () {

        dataFac.deactivateTsaFlow() //reading the .json file
            .success(function (data) {
                console.log("deactivated");

            })
            .error(function (data) {
                console.log(data)

            });

    };






    $scope.checkcode=function (data) {
        var veruser= $.param({
            app_key : "e7a8yqonomo7o6a4u6o8",
            code : $scope.vcode,
            api_key : "f642c1cd3145f4c172e7668e7a2cec3feefc52aa",
            phone : data
        });
        var fcurl=purl+veruser.app_key+"/verify";


        $http.post(fcurl,veruser,config)
        //post data to the api
            .success(function(data, status, headers, config) {
                if(data.status=="SUCCESS")
                {
                    $scope.deactivatelflow();
                    window.location="/dashboard/"
                }
                else if(data.status=="ERROR")
                {
                    swal(data.message,"","error")
                }
                else {
                    swal("Something Went Wrong","","error")
                }
                //alert from api response


            }).error(function(data, status, headers, config) {
            console.log(data);

        });

    };


});
todoAppModule.controller("loginCtrl", function($scope, $http,dataFac) {


    $scope.tsaactivate=function () {

        dataFac.activateTsaFlow() //reading the .json file
            .success(function (data) {
                console.log(data);
            })
            .error(function (data) {
                console.log(data);

            });

    };
    $scope.checklflow=function () {
        $http.get("/api/settings/gettsa/") //reading the json file
            .success(function (data) {


                $scope.tsao=data;
                if($scope.tsao.tsa==true){ //checking two step authentication enabled or not


                    $scope.tsaactivate();
                    window.location="/dashboard/authen/";




                }
                else
                {
                    window.location="/dashboard/index/";
                }


            })
            .error(function (data, status) {
                console.error("failure loading  record", status, data);
                //return blank record if something goes wrong
            });

    };
    $scope.checklflow();




});
todoAppModule.controller("settingsCtrl", function($scope, $http,dataFac,$window,$location,checktsa) {

    $scope.tsa_indicator=null;
    $scope.loader=null;

    checktsa.getTsa();
    $scope.changeemail = function() {  //change email fucntion
        $scope.loader="disabled";
        dataFac.changeEmail($scope.euser)
        //post data to the api

            .success(function(data) {
                $scope.loader=null;
                $scope.euser.email=null;

                swal(data.message,"",data.code); //alert from api response


            }).error(function(data) {
            console.log(data);
        });

    };
    $scope.changepass = function() {  //change password
        if($scope.puser.new==$scope.puser.rnew) {
            $scope.loader="disabled";
            dataFac.changePassword($scope.puser)
            //post data to the api
                .success(function (data) {
                    $scope.loader=null;
                    swal(data.message, "", data.code); //alert from api response


                }).error(function (data) {
                console.log(data);

            });


        }else
        {
            swal("Passwords Don't Match","","info") //if passwords dont match
        }
    };
    $scope.gettsastatus = function() {
        $http.get("/api/task/gettsa/") //reading the json file
            .success(function (data) {
                console.log("invoked");
                $scope.tsastatus = data;

                if($scope.tsastatus.status=="true")
                {
                    $("#tsa_stat").bootstrapToggle("on");

                }
                else
                {
                    $("#tsa_stat").bootstrapToggle("off");
                }
                // binding the data to the $scope variable
            })
            .error(function (data, status) {
                console.error("failure loading status", status, data);
                $scope.tsastatus = {}; //return blank record if something goes wrong
            });
    };
    $scope.changetsa=function () {

        $http.post("/api/task/changetsastat/",$scope.tsa_indicator)
        //post data to the api
            .success(function (data, status, headers, config) {
                if(data.code=="sccuess")
                {
                    swal(data.message,"",data.code);
                    $scope.gettsastatus();
                }
                else
                {
                    swal(data.message,"",data.code);
                    $scope.gettsastatus();
                }





            }).error(function (data, status, headers, config) {
            swal(data.message, "", data.code);
        });

    };
    $scope.changephone=function () {
        $scope.loader="disabled";
        dataFac.changePhoneno($scope.puser).success(function(data) {
            $scope.loader=null;
            swal(data.message,"",data.code); //alert from api response


        }).error(function(data) {
            console.log(data);

        });
    };
    $scope.deactivate = function() {  //deativate the user account
        //sweetaleart dialog for interface
        swal({
            title: "Are you sure?",
            text: "Once deleted, you will not be able to recover your account!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
            .then((willDelete) => {
            if (willDelete) {
                $scope.loader="disabled";
                dataFac.deactivateAccount($scope.drec)
                //post data to the api
                    .success(function(data) {

                        $scope.loader=null;
                        swal(data.message,"",data.code);
                        $window.location.reload();


                    }).error(function(data) {
                        console.log(data);

                });


            } else {
                swal("Your account is safe");
    }
    });



    };





});


