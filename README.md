# TodoApp

This application allows users to keep track of tasks. a priority level (1 through 3, with 3 being the highest priority), and a boolean value indicating whether the task has been completed or not. Every task can be edited or deleted.
You can also keep a watch on your productivity by using the applications productivity Graph feature .It also has a additional security layer in the form of two step authentication


## Deployment Link 

see TodoApp in action :-[TodoApp](http://todo54.herokuapp.com/)

### Prerequisites

You need the following for the testing and production purposes

openjdk-1.8.0 (Recomended)

Grails 3.3.0

## Built With

* Grails 3.0 
* Sweetaleart
* Angular js
* Boostrap 3.0


## Authors

* **Priyanshu Kumar** - *Initial work* - [Priyanshu966106](https://bitbucket.org/priyanshu966106)

## Other Documents

*[Rest Api Documentation](http://www.rtgstudios.in/api/api.pdf)

